Overview:

Mail Role grants roles to users based on their email address. A site developer
can configure a list of domain names to detect and a user role to grant. Mail
Role then examines a user's email address and if it determines that the email
addresses' domain name matches one of the configured domain names, it will
grant the user the configured role.

Possible scenario:

Assume we have a content rich site with a proper permission system set up.
Roles are created to grant certain permissions to certain content types.
Further assume that we want users from a certain company, say ACME, to be
granted one of those roles whenever they register. It would be a boring
process to grant each user that role manually.

That's where Mail Role comes in. Using a simple configuration approach, we can
configure it to detect all email addresses ending in @acme.com and it will do
the rest.
