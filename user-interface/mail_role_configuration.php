<?php

/**
 * @file
 * Mail Role module configuration definition file.
 */

/**
 * Configuration Form.
 *
 * @return array
 *   A collection, adhering to Drupal's form API.
 */
function _mail_role_configuration_form() {
  $config_domain_name = MailRoleConfiguration::getDomainName();
  $config_log_mode = MailRoleConfiguration::isLoggingEnabled();
  $config_user_role = MailRoleConfiguration::getUserRole();

  $user_role_option = MailRoleConfiguration::buildUserRoleOption();
  $form = array(
    'standard_configuration' => array(
      '#type' => 'fieldset',
      '#title' => t('Standard configuration'),
      '#collapsed' => FALSE,
      '#collapsible' => FALSE,
      MailRoleConfiguration::DOMAIN_NAMES_OPTION => array(
        '#type' => 'textarea',
        '#title' => t('Domain names'),
        '#description' => t('Enter the domain names, without the preceding '
          . '"@", to detect. Enter one domain name per line. For example, '
          . '"drupal.org" is correct while "@drupal.org" and "me@drupal.org" '
          . 'are incorrect.'),
        '#default_value' => $config_domain_name,
        '#required' => TRUE,
        '#rows' => 5
      ),
      MailRoleConfiguration::USER_ROLE_OPTION => array(
        '#type' => 'radios',
        '#title' => t('User role'),
        '#description' => t('Select the user role to grant.'),
        '#default_value' => $config_user_role,
        '#options' => $user_role_option,
        '#required' => TRUE
      )
    ),
    'log_configuration' => array(
      '#type' => 'fieldset',
      '#title' => t('Log configuration'),
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
      MailRoleConfiguration::LOG_MODE_OPTION => array(
        '#type' => 'checkbox',
        '#title' => t('Enable log'),
        '#description' => t('Determine whether or not the module\'s activity '
          . 'should be logged to the system log.'),
        '#default_value' => $config_log_mode,
        '#required' => FALSE
      )
    )
  );

  $form = system_settings_form($form);
  return $form;
}
