<?php

/**
 * @file
 * Mail Role module configuration API file.
 */

/**
 * Mail Role Configuration API.
 */
final class MailRoleConfiguration {
  /**
   * Email Address Patterns Option.
   *
   * @var string
   */
  const DOMAIN_NAMES_OPTION = 'mail_role_domain_names';

  /**
   * Log Mode Option.
   *
   * @var string
   */
  const LOG_MODE_OPTION = 'mail_role_log_mode';

  /**
   * User Role Option.
   *
   * @var string
   */
  const USER_ROLE_OPTION = 'mail_role_user_role';

  /**
   * Build User Role Option.
   *
   * @return array
   *   A key/value pair collection of user roles.
   */
  public static function buildUserRoleOption() {
    $user_roles =& drupal_static(__CLASS__ . __METHOD__);
    $is_user_roles_set = isset($user_roles);
    if ($is_user_roles_set === FALSE) {
      $user_roles = user_roles();
      $user_roles = drupal_map_assoc($user_roles);
    }

    return $user_roles;
  }

  /**
   * Get Configured Domain Name.
   *
   * @return string
   *   The configured domain name.
   */
  public static function getDomainName() {
    $email_address_pattern = variable_get(self::DOMAIN_NAMES_OPTION);
    return $email_address_pattern;
  }

  /**
   * Get Configured Domain Names.
   *
   * @return array
   *   A collection of configured domain names.
   */
  public static function getDomainNames() {
    $domain_names =& drupal_static(__CLASS__ . __METHOD__);
    $is_domain_names_set = isset($domain_names);
    if ($is_domain_names_set === FALSE) {
      $domain_name = self::getDomainName();
      $domain_names = explode("\n", $domain_name);

      foreach ($domain_names as $key => $value) {
        $domain_names[$key] = check_plain($value);
      }
    }

    return $domain_names;
  }

  /**
   * Get Configured User Role.
   *
   * @return string
   *   The configured user role.
   */
  public static function getUserRole() {
    $user_role = variable_get(self::USER_ROLE_OPTION);
    return $user_role;
  }

  /**
   * Determine if a Domain Name is Configured.
   *
   * @param $domain_name
   *   A domain name.
   *
   * @return bool
   *   A boolean true if the domain name is configured. A boolean false
   *   otherwise.
   */
  public static function isDomainNameConfigured($domain_name) {
    $domain_names = self::getDomainNames();
    $is_domain_name_configured = in_array($domain_name, $domain_names, TRUE);

    return $is_domain_name_configured;
  }

  /**
   * Determine if Logging is Enabled.
   *
   * @return bool
   *   A boolean true if logging is enabled. A boolean false otherwise.
   */
  public static function isLoggingEnabled() {
    $log_mode = (bool) variable_get(self::LOG_MODE_OPTION);
    return $log_mode;
  }
}
