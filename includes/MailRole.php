<?php

/**
 * @file
 * Mail role module API file.
 */

/**
 * Mail Role API.
 */
final class MailRole {
  /**
   * Log a User Role Has Been Granted.
   *
   * @param stdClass $account
   *   A user.
   * @param stdClass $role
   *   A role.
   */
  public static function logUserRoleGranted(stdClass $account, stdClass $role) {
    $log_message = 'A user (@uid:@user_name) has been granted a role (@rid:@role_name).';
    $log_variables = array(
      '@uid' => @$account->uid,
      '@user_name' => @$account->name,
      '@rid' => @$role->rid,
      '@role_name' => @$role->name
    );

    watchdog('mail_role', $log_message, $log_variables, WATCHDOG_INFO);
  }

  /**
   * Determine if a User Has a Role.
   *
   * @param stdClass $account
   *   A user.
   * @param stdClass $role
   *   A role.
   *
   * @return bool
   *   A boolean true if the user has the role. A boolean false otherwise.
   */
  public static function userHasRole(stdClass $account, stdClass $role) {
    $user_has_role = array_key_exists(@$role->rid, @$account->roles);
    return $user_has_role;
  }
}
